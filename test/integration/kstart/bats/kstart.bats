#!/usr/bin/env bats

@test "k5start binary is found in PATH" {
  run which k5start
  [ "$status" -eq 0 ]
}

@test "krenew binary is found in PATH" {
  run which krenew
  [ "$status" -eq 0 ]
}
