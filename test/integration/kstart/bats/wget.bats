#!/usr/bin/env bats

@test "wget binary is found in PATH" {
  run which wget
  [ "$status" -eq 0 ]
}
