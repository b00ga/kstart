node default {

  $afsrepobase = "http://install.linux.ncsu.edu/pub/yum/itecs/public/openafs"
  $afsreleaserpm = "openafs-release-1.0-1.noarch.rpm"

  include stdlib

  Class['epel'] -> Package<| |>
  include epel

  package { 'krb5-workstation':
    ensure	=> installed
  }
  file_line { 'enable-dns-kdc-lookup':
    path	=> '/etc/krb5.conf',
    line	=> ' dns_lookup_kdc = true',
    match	=> 'dns_lookup_kdc = false',
    after	=> 'libdefaults',
    require	=> Package['krb5-workstation']
  }


  package { 'kstart':
    ensure	=> installed
  }
  # Package crypto-policies drops a symlink in /etc/krb5.conf.d/crypto-policies
  # In the default F23 package, there seems to be a bug where symlink target
  # (/etc/crypto-policies/back-ends/krb5.config) doesn't get installed.
  # This causes kinit et all to fail to read Kerneros config.
  # Latest package appears to resolve.
  if $::operatingsystem == 'Fedora' and $::operatingsystemrelease == '23' {
     package { 'crypto-policies':
       ensure	=> latest,
       install_options => "--best",
     }
  }

  if $::osfamily == 'RedHat' {
     if $::operatingsystem == 'Fedora' {
        $loweros = downcase($::operatingsystem)
	$afsservice = "openafs-client"
     } else {
	$loweros = 'rhel'
	$afsservice = $::operatingsystemmajrelease ? {
	   '5'		=> 'afs',
	   '6'		=> 'openafs',
	   '7'		=> 'openafs-client',
	}
     }
     $afspkgdir = "$loweros$::operatingsystemmajrelease"
     $afsreleaseurl = "${afsrepobase}/${afspkgdir}/noarch/${afsreleaserpm}"
  }

  #notice("AFS repo url is ${afsreleaseurl}")
  package { "openafs-release":
    ensure	=> installed,
    provider	=> 'rpm',
    source	=> "${afsreleaseurl}"
  }
  package { "kernel-devel-$::kernelrelease":
    ensure	=> installed
  }
  package { "kernel-headers-$::kernelrelease":
    ensure	=> installed
  }
  package { "openafs-client":
    ensure	=> installed,
    require	=> [ Package['openafs-release'],Package["kernel-devel-$::kernelrelease"],Package["kernel-headers-$::kernelrelease"] ]
  }

$rhel5_init_patch='197,201c197,198
< 	if [ -f /var/lock/subsys/afs ]; then
< 	    echo $"AFS appears to be running"
< 	else
< 	    echo $"AFS does not appear to be running"
< 	fi
---
> 	status afsd
> 	RETVAL=$?
'

  if $::osfamily == 'RedHat' and $::operatingsystemmajrelease == '5' {
     include patch
     patch::file { 'fix-afs-init-status-cmd':
	target		=> '/etc/init.d/afs',
	diff_content	=> $rhel5_init_patch,
	before		=> Service["$afsservice"],
    }
  }


  service { "$afsservice":
    ensure	=> running,
    enable	=> true,
    require	=> Package['openafs-client']
  }

}
