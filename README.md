Kerberos/OpenAFS/kstart test harness
====================================

This repo contains a test harness using vagrant, test-kitchen and puppet to
stand up a virtualized test environment to test updates to the kstart package.

The k5start/krenew commands that come with kstart can be used to automate 
Kerberos tasks and support integrating with OpenAFS.

This environment installs the necessary Kerberos and OpenAFS bits to test. You
need a valid Kerberos V and OpenAFS account already to test. The OpenAFS packages
come from the NC State repos. Puppet for RHEL-like distros comes from the 
PuppetLabs Puppet Collections repo and on Fedora from the OS repos.

To run this test harness you need:

* VirtualBox (tested with 5.0.10)
* vagrant (test with 1.7.4)
* A reasonable ruby install (probably at least 1.9+, 2.x preferable).
* The following ruby gems installed:  test-kitchen kitchen-vagrant vagrant-puppet librarian-puppet
    + tested with test-kitchen 1.4.2
    + kitchen-vagrant 0.19.0
    + kitchen-puppet 1.0.35
    + librarian-puppet 2.2.1
* librarian-puppet also needs a version of puppet installed to call to install the desired puppet modules